#include <iostream>

using namespace std;

#define N 10

// U nizu od N cijelih brojeva, nalaze se duplikati. Pronadi ih i ispi�si koliko puta se
// ponavljaju! Mogu�ce su samo vrijednosti 1-9

void duplikati(int *niz) {
	int pojavljivanja[9] = { 0 };
	int indeks;

	for (int i = 0; i < N; i++) {
		indeks = niz[i] - 1;
		pojavljivanja[indeks] += 1;
	}

	for (int i = 0; i < 9; i++) {
		if (pojavljivanja[i]) {
			cout << "Broj " << i + 1 << " se pojavljuje " << pojavljivanja[i] << " puta" << endl;
		}
	}
}

int main() {
	int niz[N] = { 1, 2, 2, 5, 3, 2, 7, 9, 3, 5 };
	duplikati(niz);
}