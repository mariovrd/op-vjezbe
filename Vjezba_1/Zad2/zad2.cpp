#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

struct Student {
	int ID;
	string ime, spol;
	int kviz1, kviz2;
	int mid_term;
	int final;
	int total;
};

void dodaj_novi_zapis(Student *niz, int velicina, int ID, string ime, string spol, int kviz1, int kviz2, int mid_term, int final) {
	int i;
	for (i = 0; i < velicina; i++) {
		if (niz[i].ID == ID && !niz[i].ime.empty()) {
			cout << "Greska! Student sa tim ID-em vec postoji!" << endl;
			return;
		}
		else if (niz[i].ime.empty()) {
			break;
		}
	}
	niz[i].ID = ID;
	niz[i].ime = ime;
	niz[i].spol = spol;
	niz[i].kviz1 = kviz1;
	niz[i].kviz2 = kviz2;
	niz[i].mid_term = mid_term;
	niz[i].final = final;
	niz[i].total = kviz1 + kviz2 + mid_term + final;
	cout << "Dodan student " << ime << " (" << ID << ")" << endl;
}

void ukloni_zapis(Student *niz, int velicina, int ID) {
	int i;
	for (i = 0; i < velicina; i++) {
		if (niz[i].ID == ID) {
			cout << "Uklonjen student sa ID-em " << niz[i].ID << endl;
			niz[i].ID = NULL;
			niz[i].ime.clear();
			niz[i].spol.clear();
			niz[i].kviz1 = NULL;
			niz[i].kviz2 = NULL;
			niz[i].mid_term = NULL;
			niz[i].final = NULL;
			niz[i].total = NULL;
			return;
		}
	}
	cout << "Ne postoji student sa ID-em " << ID << endl;
}

void azuriraj_zapis(Student *niz, int velicina, int ID) {
	cout << "Upisite sto zelite azurirati: \n1 - ime\n2 - spol\n3 - kviz1\n4 - kviz2\n5 - mid_term\n6 - final" << endl;
	int i;
	for (i = 0; i < velicina; i++) {
		if (niz[i].ID == ID && !niz[i].ime.empty()) {
			break;
		}
	}
	int x;
	cin >> x;
	int novi;
	int razlika; // za update totala
	string novo;
	switch (x)
	{
	case 1:
		cout << "Upisite novo ime: " << endl;
		cin >> novo;
		niz[i].ime = novo;
		break;
	case 2:
		cout << "Upisite novi spol: " << endl;
		cin >> novo;
		niz[i].spol = novo;
		break;
	case 3:
		cout << "Upisite novi rezultat kviza 1: " << endl;
		cin >> novi;
		razlika = novi - niz[i].kviz1;
		niz[i].kviz1 = novi;
		niz[i].total += razlika;
		break;
	case 4:
		cout << "Upisite novi rezultat kviza 2: " << endl;
		cin >> novi;
		razlika = novi - niz[i].kviz2;
		niz[i].kviz2 = novi;
		niz[i].total += razlika;
		break;
	case 5:
		cout << "Upisite novi rezultat mid-terma: " << endl;
		cin >> novi;
		razlika = novi - niz[i].mid_term;
		niz[i].mid_term = novi;
		niz[i].total += razlika;
		break;
	case 6:
		cout << "Upisite novi rezultat final: " << endl;
		cin >> novi;
		razlika = novi - niz[i].final;
		niz[i].final = novi;
		niz[i].total += razlika;
		break;
	default:
		break;
	}
}

void prikazi_sve_zapise(Student *niz, int velicina) {
	for (int i = 0; i < velicina; i++) {
		if (!niz[i].ime.empty()) {
			cout << niz[i].ID << " " << niz[i].ime << " " << niz[i].spol << " " << niz[i].kviz1 << " " << niz[i].kviz2 << " " << niz[i].mid_term << " " << niz[i].final << " " << niz[i].total << endl;
		}
	}
}

void izracunaj_prosjek(Student *niz, int velicina, int ID) {
	for (int i = 0; i < velicina; i++) {
		if (niz[i].ID == ID && !niz[i].ime.empty()) {
			double prosjek;
			prosjek = (niz[i].kviz1 + niz[i].kviz2 + niz[i].mid_term + niz[i].final) / 4.0;
			cout << "Prosjek studenta " << niz[i].ime << " je " << prosjek << endl;
			return;
		}
	}
	cout << "Student s ID-em " << ID << " ne postoji" << endl;
}

void najvise_bodova(Student *niz, int velicina) {
	int max_bodova = niz[0].total;
	int max_indeks = 0;
	for (int i = 1; i < velicina; i++) {
		if (!niz[i].ime.empty()) {
			if (niz[i].total > max_bodova) {
				max_bodova = niz[i].total;
				max_indeks = i;
			}
		}
	}
	cout << "Student " << niz[max_indeks].ime << " ima najveci broj bodova (" << max_bodova << ")" << endl;
}

void najmanje_bodova(Student *niz, int velicina) {
	int min_bodova = niz[0].total;
	int min_indeks = 0;
	for (int i = 1; i < velicina; i++) {
		if (!niz[i].ime.empty()) {
			if (niz[i].total < min_bodova) {
				min_bodova = niz[i].total;
				min_indeks = i;
			}
		}
	}
	cout << "Student " << niz[min_indeks].ime << " ima najmanji broj bodova (" << min_bodova << ")" << endl;
}

void pronadi_po_id(Student *niz, int velicina, int ID) {
	for (int i = 0; i < velicina; i++) {
		if (niz[i].ID == ID && !niz[i].ime.empty()) {
			cout << niz[i].ID << " " << niz[i].ime << " " << niz[i].spol << " " << niz[i].kviz1 << " " << niz[i].kviz2 << " " << niz[i].mid_term << " " << niz[i].final << " " << niz[i].total << endl;
			return;
		}
	}
	cout << "Greska! Ne postoji student s ID-em " << ID << endl;
}

bool student_compare(Student trenutni, Student sljedeci) {
	return trenutni.total > sljedeci.total;
}

void sortiraj_po_bodovima(Student *niz, int velicina) {
	sort(niz, niz + 20, student_compare);
	prikazi_sve_zapise(niz, velicina);
}

int main() {
	Student niz[20] = { NULL };
	int velicina = sizeof(niz) / sizeof(niz[0]);

	int ID;
	string ime, spol;
	int kviz1, kviz2, mid_term, final;

	int to_do;

	do {
		cout << "\nUpisite sto zelite napraviti: " << endl << "1 - Dodaj novi zapis" << endl <<
			"2 - Ukloni zapis" << endl << "3 - Azuriraj zapis" << endl << "4 - Prikazi sve zapise" << endl <<
			"5 - Izracunaj prosjek za studenta" << endl << "6 - Prikazi studenta s najvise bodova" << endl <<
			"7 - Prikazi studenta s najmanje bodova" << endl << "8 - Pronadi studenta po ID-u" << endl <<
			"9 - Sortiraj zapise po broju bodova" << endl << "0 - Izlaz" << endl;

		cin >> to_do;

		switch (to_do)
		{
		case 1: // dodaj zapis
			cout << "Unesite ID, ime, spol, rezultate kviza1, kviza2, mid_term i final." << endl <<
				"ID ime spol rez_kviz1 rez_kviz2 mid_term final" << endl;
			cin >> ID >> ime >> spol >> kviz1 >> kviz2 >> mid_term >> final;
			dodaj_novi_zapis(niz, velicina, ID, ime, spol, kviz1, kviz2, mid_term, final);
			break;
		case 2: // ukloni zapis
			cout << "Unesite ID studenta kojeg zelite ukloniti: " << endl;
			cin >> ID;
			ukloni_zapis(niz, velicina, ID);
			break;
		case 3: // azuriraj zapis
			cout << "Unesite ID studenta ciji zapis zelite azurirati: " << endl;
			cin >> ID;
			azuriraj_zapis(niz, velicina, ID);
			break;
		case 4: // prikazi sve zapise
			prikazi_sve_zapise(niz, velicina);
			break;
		case 5: // izracunaj prosjek
			cout << "Unesite ID studenta ciji prosjek zelite izracunati: " << endl;
			cin >> ID;
			izracunaj_prosjek(niz, velicina, ID);
			break;
		case 6: // najvise bodova
			najvise_bodova(niz, velicina);
			break;
		case 7: // najmanje bodova
			najmanje_bodova(niz, velicina);
			break;
		case 8: // pronadi po ID-u
			cout << "Upisite ID studenta kojeg zelite pronaci: " << endl;
			cin >> ID;
			pronadi_po_id(niz, velicina, ID);
			break;
		case 9: // sortiraj po broju bodova
			sortiraj_po_bodovima(niz, velicina);
			break;
		case 0:
			cout << "Napustili ste izbornik." << endl;
			break;
		default:
			break;
		}
	} while (to_do != 0);
}