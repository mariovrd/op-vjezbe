#include <iostream>

using namespace std;

int& pronadi(int *niz, int velicina) {
	for (int i = 0; i < velicina; i++) {
		int jedinice, stotice;
		jedinice = niz[i] % 10;
		stotice = (niz[i] / 100) % 10;
		if ((jedinice + stotice) == 5) {
			return niz[i];
		}
	}
}

int main() {
	int niz[] = { 4321, 5245, 1345, 9875, 4322, 6543 };
	int velicina = sizeof(niz) / sizeof(niz[0]);

	cout << ++pronadi(niz, velicina) << endl;
}