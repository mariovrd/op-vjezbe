#ifndef LANDVEHICLE_H
#define LANDVEHICLE_H

#include <string>
#include "Vehicle.h"

namespace OSS {
	class LandVehicle : virtual public Vehicle {
	protected:
		std::string typeOfVehicle;
		unsigned numOfPassengers;

	public:
		LandVehicle();
		std::string Type();
		virtual ~LandVehicle();
	};
}
#endif