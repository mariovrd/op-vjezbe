#ifndef WATERCRAFT_H
#define WATERCRAFT_H

#include <string>
#include "Vehicle.h"

namespace OSS {
	class Watercraft : virtual public Vehicle {
	protected:
		std::string typeOfVehicle;
		unsigned numOfPassengers;

	public:
		Watercraft();
		std::string Type();
		virtual ~Watercraft();
 	};
}
#endif