#ifndef CAR_H
#define CAR_H

#include <string>
#include "LandVehicle.h"

namespace OSS {
	class Car : public LandVehicle {
	protected:

	public:
		Car();
		unsigned Passengers();
	};
}
#endif