#include "Bike.h"

namespace OSS {
	Bike::Bike() {
		this->numOfPassengers = 1;
	}

	unsigned Bike::Passengers() {
		return numOfPassengers;
	}
}