#include "Watercraft.h"

namespace OSS {
	Watercraft::Watercraft() {
		this->typeOfVehicle = "water";
	}

	std::string Watercraft::Type() {
		return typeOfVehicle;
	}

	Watercraft::~Watercraft() {

	}
}