#ifndef CATAMARAN_H
#define CATAMARAN_H

#include <string>
#include "Watercraft.h"

namespace OSS {
	class Catamaran : public Watercraft {
	protected:

	public:
		Catamaran(unsigned numOfPassengers);
		unsigned Passengers();
	};
}
#endif