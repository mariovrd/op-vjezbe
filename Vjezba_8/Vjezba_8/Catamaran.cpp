#include "Catamaran.h"

namespace OSS {
	Catamaran::Catamaran(unsigned numOfPassengers) {
		this->numOfPassengers = numOfPassengers;
	}

	unsigned Catamaran::Passengers() {
		return numOfPassengers;
	}
}