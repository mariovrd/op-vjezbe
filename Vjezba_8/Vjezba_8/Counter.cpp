#include "Counter.h"
#include <iostream>

Counter::Counter() {
	totalPassengers = 0;
}

void Counter::Add(OSS::Vehicle *v)
{
	this->totalPassengers += v->Passengers();
	std::cout << v->Type() << ", putnika: " << v->Passengers() << std::endl;
}

int Counter::Total() {
	return totalPassengers;
}