#include "Ferry.h"
#include <iostream>

namespace OSS {
	Ferry::Ferry(unsigned numOfPassengers, unsigned numOfBikes, unsigned numOfVehicles) {
		this->numOfPassengers = numOfPassengers;
		this->numOfBikes = numOfBikes;
		this->numOfVehicles = numOfVehicles;
	}

	unsigned Ferry::Passengers() {
		return this->numOfPassengers + this->numOfBikes + (this->numOfVehicles * 5);
	}
}