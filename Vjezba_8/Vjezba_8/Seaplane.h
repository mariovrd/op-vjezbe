#ifndef SEAPLANE_H
#define SEAPLANE_H

#include <string>
#include "Watercraft.h"
#include "Aircraft.h"

namespace OSS {
	class Seaplane : public Watercraft, public Aircraft {
	protected:
		std::string typeOfVehicle;
		unsigned numOfPassengers;

	public:
		Seaplane(unsigned numOfPassengers);
		unsigned Passengers();
		std::string Type();
	};
}
#endif