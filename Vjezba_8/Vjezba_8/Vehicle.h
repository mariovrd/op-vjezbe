#ifndef VEHICLE_H
#define VEHICLE_H

#include <string>

namespace OSS {
	class Vehicle {

	public:
		virtual std::string Type() = 0; // vrsta prijevoza (land, water, air)
		virtual unsigned Passengers() = 0;
		virtual ~Vehicle() {};
	};
}
#endif