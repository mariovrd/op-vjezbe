#ifndef BIKE_H
#define BIKE_H

#include <string>
#include "LandVehicle.h"

namespace OSS {
	class Bike : public LandVehicle {
	protected:

	public:
		Bike();
		unsigned Passengers();
	};
}
#endif