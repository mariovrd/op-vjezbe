#include "LandVehicle.h"

namespace OSS {
	LandVehicle::LandVehicle() {
		this->typeOfVehicle = "land";
	}

	std::string LandVehicle::Type() {
		return typeOfVehicle;
	}

	LandVehicle::~LandVehicle() {

	}

}