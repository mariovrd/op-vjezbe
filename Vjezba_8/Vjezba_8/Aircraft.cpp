#include "Aircraft.h"

namespace OSS {
	Aircraft::Aircraft() {
		this->typeOfVehicle = "air";
	}

	std::string Aircraft::Type() {
		return typeOfVehicle;
	}

	Aircraft::~Aircraft() {

	}
}