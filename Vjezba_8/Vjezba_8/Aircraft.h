#ifndef AIRCRAFT_H
#define AIRCRAFT_H

#include <string>
#include "Vehicle.h"

namespace OSS {
	class Aircraft : virtual public Vehicle {
	protected:
		std::string typeOfVehicle;
		unsigned numOfPassengers;

	public:
		Aircraft();
		std::string Type();
		virtual ~Aircraft();
	};
}
#endif