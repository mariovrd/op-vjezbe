#ifndef FERRY_H
#define FERRY_H

#include <string>
#include "Watercraft.h"

namespace OSS {
	class Ferry : public Watercraft {
	protected:
		unsigned numOfBikes;
		unsigned numOfVehicles;

	public:
		Ferry(unsigned numOfPassengers, unsigned numOfBikes, unsigned numOfVehicles);
		unsigned Passengers();
	};
}
#endif