#include <iostream>
#include "Bike.h"
#include "Car.h"
#include "Catamaran.h"
#include "Ferry.h"
#include "Seaplane.h"
#include "Counter.h"
//#include "Vehicle.h"

int main(void)
{
	using namespace OSS;
	Counter c;
	Vehicle* v[] = { new Bike, new Car, new Catamaran(30), new Ferry(10, 5, 3), new Seaplane(15) };
	size_t sz = sizeof v / sizeof v[0];
	for (unsigned i = 0; i < sz; ++i)
		c.Add(v[i]);
	std::cout << "ukupno " << c.Total() << " putnika" << std::endl;
	for (unsigned i = 0; i < sz; ++i)
		delete v[i];
}