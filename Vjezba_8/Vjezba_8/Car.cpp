#include "Car.h"

namespace OSS {
	Car::Car() {
		this->numOfPassengers = 5;
	}

	unsigned Car::Passengers() {
		return numOfPassengers;
	}
}