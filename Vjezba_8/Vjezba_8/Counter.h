#ifndef COUNTER_H
#define COUNTER_H

#include "Vehicle.h"

class Counter {
protected:
	int totalPassengers;
public:
	Counter();
	void Add(OSS::Vehicle*);
	int Total();
};

#endif