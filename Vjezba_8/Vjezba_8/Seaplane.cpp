#include "Seaplane.h"

namespace OSS {
	Seaplane::Seaplane(unsigned numOfPassengers) {
		this->typeOfVehicle = "air-water";
		this->numOfPassengers = numOfPassengers;
	}

	unsigned Seaplane::Passengers() {
		return numOfPassengers;
	}

	std::string Seaplane::Type() {
		return typeOfVehicle;
	}
}