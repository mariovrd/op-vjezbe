#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

void print_vector(vector<string> theVec) {
	for (auto it = theVec.begin(); it != theVec.end(); it++) {
		cout << *it << endl;
	}
}

bool check_str(string theString) {
	if (theString.size() > 20)
		return false;

	for (int i = 0; i < (int)theString.size(); i++) {
		if (!isdigit(theString[i])) {
			if (islower(theString[i])) {
				cout << theString[i] << " ne zadovoljava uvjet" << endl;
				return false;
			}
		}
	}
	return true;
}

int get_num_of_chars(string& theString, int index) {
	int numOfChars = 0;
	char ch = theString[index];
	int i = index;
	while (theString[i] == ch) {
		numOfChars++;
		i++;
	}
	return numOfChars;
}

void letters_to_numbers(string& theString) {
	for (int j = 0; j < theString.size(); j++) {
		int numOfChars = get_num_of_chars(theString, j);
		char ch = theString.at(j);
		if (numOfChars > 1) {
			theString.erase(j, numOfChars);
			string broj = to_string(numOfChars);
			theString.insert(j, broj);
			theString.insert(theString.begin() + j + 1, ch);
			j++;
		}
	}
}

void numbers_to_letters(string& theString) {
	int numOfChars = 0;
	char ch;
	for (int j = 0; j < theString.size(); j++) {
		if (isdigit(theString[j])) {
			numOfChars = theString[j] - '0';
			ch = theString[j + 1];
			theString.erase(j, 2);
			theString.insert(j, numOfChars, ch);
			j += numOfChars - 1;
		}
	}
}

int main() {
	int n;
	cout << "Koliko stringova zelite upisati: " << endl;
	cin >> n;

	vector<string> myVec(n);

	int i = 0;
	while (i < n) {
		string input;
		cin >> input;
		while (!check_str(input)) {
			cout << "String moze imati najvise 20 znakova (znakovi mogu biti samo brojevi ili velika slova)\n" <<
				"Unesite ponovno: " << endl;
			cin >> input;
		}
		myVec[i] = input;
		i++;
	}

	for (i = 0; i < myVec.size(); i++) {
		if (find_if(myVec[i].begin(), myVec[i].end(), ::isdigit) == myVec[i].end()) {
			letters_to_numbers(myVec[i]);
		}
		else {
			numbers_to_letters(myVec[i]);
		}
	}

	print_vector(myVec);

}