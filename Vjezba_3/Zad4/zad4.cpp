#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

void state(vector<int>& theVec) {
	cout << "Ostalo je " << theVec.size() << " sibica" << endl;
}

void computers_move(vector<int>& theVec) {
	if (theVec.size() == 1) {
		cout << "Racunalo gubi" << endl;
		theVec.pop_back();
		return;
	}

	int num;
	if (theVec.size() % 4 == 0) { // 4, 8, 12, 16, 20
		num = 3;
	}
	else if (theVec.size() % 4 == 2) { // 2, 6, 10, 14, 18
		num = 1;
	} 
	else if (theVec.size() % 4 == 3) { // 3, 7, 11, 15, 19
		num = 2;
	}
	else { // 1, 5, 9, 13, 17, 21 gubitnicke pozicije pa uzima random
		srand(time(NULL)); 
		num = 1 + (rand() % 3);
	}

	for (int i = 0; i < num; i++)
		theVec.pop_back();
	cout << "Racunalo je uzelo " << num << " sibica" << endl;
}

void players_move(vector<int>& theVec, int num) {
	if (theVec.size() == 1) {
		cout << "Igrac gubi" << endl;
		theVec.pop_back();
		return;
	}
	if (num > theVec.size()) {
		cout << "Igrac gubi" << endl;
		while (theVec.size() > 0) theVec.pop_back();
		return;
	}

	for (int i = 0; i < num; i++)
		theVec.pop_back();
	cout << "Uzeli ste " << num << " sibica" << endl;
}

int main() {
	vector<int> matches(21);
	int playersInput;

	while (matches.size() > 0) {
		computers_move(matches);
		if (matches.size() == 0) break;
		state(matches);
		cout << "Unesite koliko sibica zelite uzeti: ";
		cin >> playersInput;
		players_move(matches, playersInput);
		state(matches);
	}

}