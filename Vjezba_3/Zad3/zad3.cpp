#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

// Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .

void find_punctuation(string& theString) {
	string::iterator it;
	for (it = theString.begin(); it != theString.end() - 1; it++) {
		if (*it == '.' || *it == ',' || *it == '?' || *it == '!') {
			if (*(it - 1) == ' ' && *(it + 1) == ' ') {
				theString.erase(it - 1);
			}
			else if (*(it - 1) == ' ' && *(it + 1) != ' ') { // slucaj da fali razmak nakon , .
				theString.insert(it + 1, ' ');
				theString.erase(it - 1);
			}
			else if (*(it - 1) != ' ' && *(it + 1) != ' ') {
				theString.insert(it + 1, ' ');
			}
		}
	}
	if (*(it - 1) == ' ' && (*it == '.' || *it == ',' || *it == '?' || *it == '!'))
		theString.erase(it - 1);
}

int main() {
	string input = "Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .";
	cout << input << endl;
	find_punctuation(input);
	cout << input << endl;
}