#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

struct Producent {
	string name;
	string movie;
	int year;
};

void print_vector(vector<Producent> theVec) {
	cout << "IME\tFILM\tGODINA" << endl;
	for (auto it = theVec.begin(); it != theVec.end(); ++it) {
		cout << it->name << "\t" << it->movie << "\t" << it->year << endl;
	}
}

Producent enter_producer() {
	string name, movie;
	int year;

	cin.ignore();
	cout << "Unesite ime: ";
	getline(cin, name);
	cout << "Unesite naziv filma: ";
	getline(cin, movie);
	cout << "Unesite godinu: ";
	cin >> year;

	Producent returnProducer = { name, movie, year };
	return returnProducer;
}

int count_by_name(vector<Producent> theVec, string name) {
	int occr = 0;

	for (auto it = theVec.begin(); it != theVec.end(); ++it) {
		if (it->name == name)
			occr++;
	}

	return occr;
}

void find_most_represented(vector<Producent> theVec) {
	map<string, int> myMap;

	for (auto it = theVec.begin(); it != theVec.end(); ++it) {
		if (myMap.find(it->name) == myMap.end()) { // ako nije u mapi, dodaj
			myMap.insert({ it->name, count_by_name(theVec, it->name) });
		}
	}

	int max = 0;
	for (auto itr = myMap.begin(); itr != myMap.end(); ++itr) {
		if (itr->second > max)
			max = itr->second;
	}

	for (auto itr = myMap.begin(); itr != myMap.end(); ++itr) {
		if (itr->second == max)
			cout << "Najzastupljeniji autor je " << itr->first << " sa " << max << " pojavljivanja" << endl;
	}
}

int main() {
	vector<Producent> myVec;

	int n;
	cout << "Upisite koliko producenata zelite dodati" << endl;
	cin >> n;

	while (n > 0) {
		Producent input = enter_producer();
		myVec.push_back(input);
		n--;
	}

	print_vector(myVec);
	cout << endl;

	find_most_represented(myVec);

}