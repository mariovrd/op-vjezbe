#include "myfunctions.h"
#include <algorithm>

vector<int> binary_vector(vector<int> firstVec, vector<int> secondVec) {
	vector<int> binaryVec;

	for (int i = 0; i < firstVec.size(); i++) {
		sort(firstVec.begin(), firstVec.end());
		sort(secondVec.begin(), secondVec.end());
		if (!binary_search(secondVec.begin(), secondVec.end(), firstVec.at(i))) {
			binaryVec.push_back(firstVec.at(i));
		}
	}
	return binaryVec;
}

int main() {
	srand(time(NULL));
	vector<int> firstVec = create_vector(0, 10, 5, false);
	vector<int> secondVec = create_vector(0, 10, 5, false);
	print_vector(firstVec);
	print_vector(secondVec);
	vector<int> binaryVec = binary_vector(firstVec, secondVec);
	print_vector(binaryVec);

}