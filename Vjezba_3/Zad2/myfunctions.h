#ifndef MYFUNCTIONS_H
#define MYFUNCTIONS_H

#include <iostream>
#include <ctime>
#include <vector>

using namespace std;

vector<int> create_vector(const int a = 0, const int b = 100, unsigned int n = 5, bool input = false);
void print_vector(vector<int>& theVec);

#endif