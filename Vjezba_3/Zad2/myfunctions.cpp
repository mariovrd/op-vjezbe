#include "myfunctions.h"

vector<int> create_vector(const int a, const int b, unsigned int n, bool input) {
	vector<int> newVec;
	if (!input) {
		while (n > 0) {
			newVec.push_back(a + (rand() % (b - a + 1)));
			n--;
		}
	}
	else {
		int input;
		while (n > 0) {
			cout << "Unesite broj: ";
			cin >> input;
			newVec.push_back(input);
			n--;
		}
	}
	return newVec;
}

void print_vector(vector<int>& theVec) {
	for (auto it = theVec.begin(); it != theVec.end(); it++)
		cout << *it << " ";
	cout << endl;
}
