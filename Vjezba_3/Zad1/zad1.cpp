#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

vector<int> create_vector(const int a = 0, const int b = 100, unsigned int n = 5, bool input = false) {
	vector<int> newVec;
	if (!input) {
		srand(time(NULL));
		while (n > 0) {
			newVec.push_back(a + (rand() % (b - a + 1)));
			n--;
		}
	}
	else {
		int input;
		while (n > 0) {
			cout << "Unesite broj: ";
			cin >> input;
			newVec.push_back(input);
			n--;
		}
	}
	return newVec;
}

void print_vector(vector<int>& theVec) {
	for (auto it = theVec.begin(); it != theVec.end(); it++)
		cout << *it << " ";
	cout << endl;
}

int main() {
	int min, max, size;
	bool input;
	cout << "Unesite donju granicu intervala: ";
	cin >> min;
	cout << "Unesite gornju granicu intervala: ";
	cin >> max;
	cout << "Unesite velicinu vektora: ";
	cin >> size;
	cout << "Unesite 0 da generirate random brojeve, a 1 ukoliko zelite vi upisivati: ";
	cin >> input;
	vector<int> myVec = create_vector(min, max, size, input);
	print_vector(myVec);
}