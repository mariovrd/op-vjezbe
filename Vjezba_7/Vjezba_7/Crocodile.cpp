#include "Crocodile.h"

using namespace std;

Crocodile::Crocodile(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
	int gestationalPeriod, double avgTemp, string wayOfReproduction, int oneMeal) : Reptile(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime,
		gestationalPeriod, avgTemp, wayOfReproduction) {
	this->oneMeal = oneMeal;
}
