#include "Bird.h"

using namespace std;

Bird::Bird(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
	int incubationTime, double avgTemp, string wayOfReproduction) : ZooAnimal(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime) {
	this->incubationTime = incubationTime;
	this->avgTemp = avgTemp;
	this->wayOfReproduction = wayOfReproduction;
}

istream& operator>>(istream& is, Bird& b) {
	cout << "Enter gestational period: ";
	is >> b.incubationTime;
	cout << "Enter average temperature: ";
	is >> b.avgTemp;
	cout << "Enter way of reproduction: ";
	is >> b.wayOfReproduction;
	return is;
}

ostream& operator<<(ostream& os, Bird& b) {
	os << "Gestational Period: " << b.incubationTime << "\nAverage temperature: " << b.avgTemp << "\nWay of reproduction: " << b.wayOfReproduction << endl;
	return os;
}