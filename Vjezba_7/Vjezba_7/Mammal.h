#ifndef MAMMAL_H
#define MAMMAL_H

#include <iostream>
#include <string>
#include "ZooAnimal.h"

class Mammal : public ZooAnimal {
protected:
	int gestationalPeriod;
	double avgTemp;
	std::string wayOfReproduction;

public:
	Mammal(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime, 
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction);
	friend std::istream& operator>>(std::istream& is, Mammal& m);
	friend std::ostream& operator<<(std::ostream& os, Mammal& m);
};

#endif