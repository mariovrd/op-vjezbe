#ifndef ELEPHANT_H
#define ELEPHANT_H

#include "Mammal.h"

class Elephant : public Mammal {
protected:
	int oneMeal;
public: 
	Elephant(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);
};

#endif // !ELEPHANT_H
