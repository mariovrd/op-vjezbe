#ifndef OWL_H
#define OWL_H

#include "Bird.h"

class Owl : public Bird {
protected:
	int oneMeal;
public:
	Owl(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);

};

#endif // !OWL_H
