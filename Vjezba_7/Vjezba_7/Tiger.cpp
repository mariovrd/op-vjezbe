#include "Tiger.h"

using namespace std;

Tiger::Tiger(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
	int gestationalPeriod, double avgTemp, string wayOfReproduction, int oneMeal) : Mammal(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime,
		gestationalPeriod, avgTemp, wayOfReproduction) {
	this->oneMeal = oneMeal;
}
