#ifndef MONKEY_H
#define MONKEY_H

#include "Mammal.h"

class Monkey : public Mammal {
protected:
	int oneMeal;
public:
	Monkey(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);

};

#endif // !MONKEY_H
