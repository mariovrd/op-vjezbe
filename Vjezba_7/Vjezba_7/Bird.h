#ifndef BIRD_H
#define BIRD_H

#include <iostream>
#include <string>
#include "ZooAnimal.h"

class Bird : public ZooAnimal {
protected:
	int incubationTime;
	double avgTemp;
	std::string wayOfReproduction;

public:
	Bird(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int incubationTime, double avgTemp, std::string wayOfReproduction);
	friend std::istream& operator>>(std::istream& is, Bird& b);
	friend std::ostream& operator<<(std::ostream& os, Bird& b);
};

#endif