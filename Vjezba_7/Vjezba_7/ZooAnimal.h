#ifndef ZOOANIMAL_H
#define ZOOANIMAL_H

#include <string>
#include <iostream>

using namespace std;

class Mass {
private:
	unsigned year;
	double weight;
public:
	Mass() {
		year = 0;
		weight = 0;
	}

	Mass(int year, double weight) {
		this->year = year;
		this->weight = weight;
	}

	void Print() const {
		cout << "Year: " << year << " - " << weight << " kg" << endl;
	}

	unsigned GetYear() const { return year; }
	double GetWeight() const { return weight; }
};

class ZooAnimal {
protected:
	string species;
	string name;
	unsigned yearOfBirth;
	unsigned numOfCages;
	unsigned numOfMeals;
	unsigned expectedLifetime;
	Mass* massData;
public:
	ZooAnimal(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime);
	~ZooAnimal();
	ZooAnimal(const ZooAnimal& other);

	string GetName() const;
	unsigned GetYearOfBirth() const;
	unsigned GetExpectedLifetime() const;
	unsigned GetNumOfMeals() const;

	void SetName(string name);

	void ChangeMeal(bool inc);

	void AddMassData(Mass& newData);

	double GetWeightChange() const;

	void PrintData() const;

	friend std::ostream& operator<<(std::ostream& os, ZooAnimal& m);
};

#endif // !ZOOANIMAL_H
