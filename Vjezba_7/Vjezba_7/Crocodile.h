#ifndef CROCODILE_H
#define CROCODILE_H

#include "Reptile.h"

class Crocodile : public Reptile {
protected:
	int oneMeal;
public:
	Crocodile(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);

};

#endif // !CROCODILE_H
