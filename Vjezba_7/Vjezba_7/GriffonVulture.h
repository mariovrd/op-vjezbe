#ifndef GRIFFONVULTURE_H
#define GRIFFONVULTURE_H

#include "Bird.h"

class GriffonVulture : public Bird {
protected:
	int oneMeal;
public:
	GriffonVulture(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);

};

#endif // !GRIFFONVULTURE_H
