#ifndef TIGER_H
#define TIGER_H

#include "Mammal.h"

class Tiger : public Mammal {
protected:
	int oneMeal;
public:
	Tiger(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);

};

#endif // !TIGER_H
