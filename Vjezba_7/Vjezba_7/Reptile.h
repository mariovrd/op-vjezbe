#ifndef REPTILE_H
#define REPTILE_H

#include <iostream>
#include <string>
#include "ZooAnimal.h"

class Reptile : public ZooAnimal {
protected:
	int incubationTime;
	int ambientTemp;
	std::string wayOfReproduction;

public:
	Reptile(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int incubationTime, double avgTemp, std::string wayOfReproduction);
	friend std::istream& operator>>(std::istream& is, Reptile& r);
	friend std::ostream& operator<<(std::ostream& os, Reptile& r);
};

#endif