#include "Mammal.h"

using namespace std;

Mammal::Mammal(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
	int gestationalPeriod, double avgTemp, string wayOfReproduction) : ZooAnimal(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime) {
	this->gestationalPeriod = gestationalPeriod;
	this->avgTemp = avgTemp;
	this->wayOfReproduction = wayOfReproduction;
}

//istream &getline(istream &in, Mammal& m)
//{
//	getline(in, m.gestationalPeriod);
//	//getline(saveFile, saveAcctName);
//	cout << "Enter gestational period: ";
//	//is >> m.gestationalPeriod;
//	getline(in, m.gestationalPeriod);
//
//	cout << "Enter average temperature: ";
//	in >> m.avgTemp;
//	cout << "Enter way of reproduction: ";
//	in >> m.wayOfReproduction;
//	return in;
//}

istream& operator>>(istream& is, Mammal& m) {
	cout << "Enter gestational period: ";
	is >> m.gestationalPeriod;
	cout << "Enter average temperature: ";
	is >> m.avgTemp;
	cout << "Enter way of reproduction: ";
	//string str;
	//getline(cin, str);
	//m.wayOfReproduction = str;
	is >> m.wayOfReproduction;
	return is;
}

ostream& operator<<(ostream& os, Mammal& m) {
	os << "Species: " << m.species << "\nName: " << m.name << "\nYear of birth: " << m.yearOfBirth <<
		"\nNumber of cages: " << m.numOfCages << "\nNumber of meals: " << m.numOfMeals <<
		"\nExpected lifetime: " << m.expectedLifetime << endl;
	os << "Gestational Period: " << m.gestationalPeriod << "\nAverage temperature: " << m.avgTemp << "\nWay of reproduction: " << m.wayOfReproduction << endl;
	return os;
}