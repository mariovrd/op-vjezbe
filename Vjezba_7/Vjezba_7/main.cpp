#include <iostream>
#include <time.h>
#include <vector>
#include "ZooAnimal.h"
#include "Mammal.h"
#include "Bird.h"
#include "Reptile.h"
#include "Crocodile.h"
#include "Elephant.h"
#include "GriffonVulture.h"
#include "Monkey.h"
#include "Owl.h"
#include "Tiger.h"
#include "Turtle.h"

void CheckWeightChange(ZooAnimal& animal) {
	double weightChange = animal.GetWeightChange();
	if (weightChange > 10) {
		cout << "Got " << weightChange << "% of kgs. ";
		animal.ChangeMeal(false);
	}
	else if (weightChange < -10) {
		cout << "Lost " << weightChange << "% of kgs. ";
		animal.ChangeMeal(true);
	}
	else {
		cout << animal.GetName() << "'s weight is perfect, don't change anything." << endl;
	}
}

vector<ZooAnimal> FillVector() {
	vector<ZooAnimal> animals;
	time_t t = time(0);
	struct tm *now = localtime(&t);
	int currentYear = now->tm_year + 1900;
	srand(time(NULL));
	unsigned n;
	string species;
	string name;
	unsigned yearOfBirth;
	unsigned numOfCages;
	unsigned numOfMeals;
	unsigned expectedLifetime;
	cout << "How many animals do you want to enter? ";
	cin >> n;


	for (int i = 0; i < n; i++) {
		cout << "Enter species: ";
		cin >> species;
		cout << "Enter name: ";
		cin >> name;
		cout << "Enter year of birth: ";
		cin >> yearOfBirth;
		cout << "Enter number of cages: ";
		cin >> numOfCages;
		cout << "Enter number of meals: ";
		cin >> numOfMeals;
		cout << "Enter expected lifetime: ";
		cin >> expectedLifetime;

		ZooAnimal animal(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime);
		animals.push_back(animal);
	}

	return animals;
}

int main() {
	//vector<ZooAnimal> animals = FillVector();

	//for (auto it = animals.begin(); it != animals.end(); it++) {
	//	for (int j = 0; j < it->GetExpectedLifetime() + 2; j++) {
	//		Mass newData(it->GetYearOfBirth() + j, rand() % 20 + 10 + j);
	//		it->AddMassData(newData);
	//	}
	//	it->PrintData();
	//	CheckWeightChange(*it);
	//	it->PrintData();
	//	cout << endl;
	//}


	Tiger *tig = new Tiger("Bengal", "Leo", 1998, 3, 5, 12, 0, 0, "", 2);
	cin >> *tig;
	cout << *tig;
	Turtle *turtle = new Turtle("Turtle", "Ena", 1996, 4, 6, 3, 0, 0, "", 1);
	cin >> *turtle;
	cout << *turtle;
	Owl *owl = new Owl("Owl", "Mickey", 2001, 2, 4, 5, 0, 0, "", 3);
	cin >> *owl;
	cout << *owl;

	vector<ZooAnimal*> animals;
	animals.push_back(tig);
	animals.push_back(turtle);
	animals.push_back(owl);

	for (int i = 0; i < animals.size(); i++) {
		cout << *animals[i] << endl;
	}

}