#ifndef TURTLE_H
#define TURTLE_H 

#include "Reptile.h"

class Turtle : public Reptile {
protected:
	int oneMeal;
public:
	Turtle(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
		int gestationalPeriod, double avgTemp, std::string wayOfReproduction, int oneMeal);

};

#endif // !TURTLE_H
