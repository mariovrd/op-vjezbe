#include "GriffonVulture.h"

using namespace std;

GriffonVulture::GriffonVulture(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
	int gestationalPeriod, double avgTemp, string wayOfReproduction, int oneMeal) : Bird(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime,
		gestationalPeriod, avgTemp, wayOfReproduction) {
	this->oneMeal = oneMeal;
}
