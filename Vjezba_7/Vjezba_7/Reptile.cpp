#include "Reptile.h"

using namespace std;

Reptile::Reptile(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime,
	int incubationTime, double avgTemp, string wayOfReproduction) : ZooAnimal(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime) {
	this->incubationTime = incubationTime;
	this->ambientTemp = ambientTemp;
	this->wayOfReproduction = wayOfReproduction;
}

istream& operator>>(istream& is, Reptile& r) {
	cout << "Enter gestational period: ";
	is >> r.incubationTime;
	cout << "Enter ambient temperature: ";
	is >> r.ambientTemp;
	cout << "Enter way of reproduction: ";
	is >> r.wayOfReproduction;
	return is;
}

ostream& operator<<(ostream& os, Reptile& r) {
	os << "Gestational Period: " << r.incubationTime << "\tAmbient temperature: " << r.ambientTemp << "\nWay of reproduction: " << r.wayOfReproduction << endl;
	return os;
}