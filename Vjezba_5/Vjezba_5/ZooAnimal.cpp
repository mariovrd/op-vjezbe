#include "ZooAnimal.h"
#include <iostream>
#include <ctime>
#include <time.h>

using namespace std;

ZooAnimal::ZooAnimal(string species, string name, unsigned yearOfBirth, unsigned numOfCages, unsigned numOfMeals, unsigned expectedLifetime) {
	cout << "Animal " << name << " constructed" << endl;
	this->species = species;
	this->name = name;
	this->yearOfBirth = yearOfBirth;
	this->numOfCages = numOfCages;
	this->numOfMeals = numOfMeals;
	this->expectedLifetime = expectedLifetime;

	this->massData = new Mass[2 * expectedLifetime];
}

ZooAnimal::~ZooAnimal() {
	cout << "Animal " << this->name << " destructed" << endl;
	delete [] this->massData;
}

ZooAnimal::ZooAnimal(const ZooAnimal& other) {
	species = other.species;
	name = other.name;
	yearOfBirth = other.yearOfBirth;
	numOfCages = other.numOfCages;
	numOfMeals = other.numOfMeals;
	expectedLifetime = other.expectedLifetime;
	massData = new Mass[2 * expectedLifetime];
	for (int i = 0; i < expectedLifetime * 2; i++) { 
		massData[i] = other.massData[i];
	}
}

string ZooAnimal::GetName() const {
	return name;
}

unsigned ZooAnimal::GetYearOfBirth() const {
	return yearOfBirth;
}

unsigned ZooAnimal::GetExpectedLifetime() const {
	return expectedLifetime;
}

void ZooAnimal::SetName(string name) {
	this->name = name;
}

void ZooAnimal::ChangeMeal(bool inc) {
	if (inc) {
		this->numOfMeals++;
		cout << name << "'s number of meals incremented." << endl;
	}
	else {
		this->numOfMeals--;
		cout << name << "'s number of meals decremented." << endl;
	}
}

void ZooAnimal::AddMassData(Mass& newData) {
	time_t t = time(0);
	struct tm *now = localtime(&t);
	int currentYear = now->tm_year + 1900;
	if (newData.GetYear() > currentYear) {
		cout << "You can't add weight for future." << endl;
		return;
	}
	for (int i = 0; i < expectedLifetime * 2; i++) {
		if (this->massData[i].GetYear() == newData.GetYear() && newData.GetYear() != currentYear) {
			cout << "There is already data for " << this->massData[i].GetYear() << " year." << endl;
			return;
		}
		else {
			if (this->massData[i].GetWeight() == 0 || this->massData[i].GetYear() == newData.GetYear()) {
				this->massData[i] = newData;
				//cout << "Weight log for " << newData.GetYear() << " added (" << newData.GetWeight() << "kg)." << endl;
				return;
			}
		}
	}
}

double ZooAnimal::GetWeightChange() const {
	time_t t = time(0);
	struct tm *now = localtime(&t);
	int currentYear = now->tm_year + 1900;

	double currentWeight = 0;
	double previousWeight = 0;
	
	for (int i = 0; i < expectedLifetime * 2; i++) {
		if (this->massData[i].GetYear() == currentYear) {
			currentWeight = this->massData[i].GetWeight();
		}
		else if (this->massData[i].GetYear() == (currentYear - 1)) {
			previousWeight = this->massData[i].GetWeight();
		}
	}

	double weightChange = (currentWeight - previousWeight) / previousWeight * 100;
	return weightChange;
}

void ZooAnimal::PrintData() const {
	cout << "Species: " << species << "\nName: " << name << "\nYear of birth: " << yearOfBirth <<
		"\nNumber of cages: " << numOfCages << "\nNumber of meals: " << numOfMeals <<
		"\nExpected lifetime: " << expectedLifetime << endl;
	
	bool print;
	cout << "Do you want to print weight logs? (1 - yes | 0 - no) ";
	cin >> print;
	if (print) {
		for (int i = 0; i < expectedLifetime * 2; i++) {
			if (this->massData[i].GetYear() == 0)
				break;
			this->massData[i].Print();
		}
	}
}