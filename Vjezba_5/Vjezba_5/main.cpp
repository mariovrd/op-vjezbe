#include <iostream>
#include <time.h>
#include <vector>
#include "ZooAnimal.h"

void CheckWeightChange(ZooAnimal& animal) {
	double weightChange = animal.GetWeightChange();
	if (weightChange > 10) {
		cout << "Got " << weightChange << "% of kgs. ";
		animal.ChangeMeal(false);
	}
	else if (weightChange < -10) {
		cout << "Lost " << weightChange << "% of kgs. ";
		animal.ChangeMeal(true);
	}
	else {
		cout << animal.GetName() <<"'s weight is perfect, don't change anything." << endl;
	}
}

int main() {
	time_t t = time(0);
	struct tm *now = localtime(&t);
	int currentYear = now->tm_year + 1900;
	srand(time(NULL));
	unsigned n;
	string species;
	string name;
	unsigned yearOfBirth;
	unsigned numOfCages;
	unsigned numOfMeals;
	unsigned expectedLifetime;

	cout << "How many animals do you want to enter? ";
	cin >> n;

	vector<ZooAnimal> animals;

	for (int i = 0; i < n; i++) {
		cout << "Enter species: ";
		cin >> species;
		cout << "Enter name: ";
		cin >> name;
		cout << "Enter year of birth: ";
		cin >> yearOfBirth;
		cout << "Enter number of cages: ";
		cin >> numOfCages;
		cout << "Enter number of meals: ";
		cin >> numOfMeals;
		cout << "Enter expected lifetime: ";
		cin >> expectedLifetime;

		ZooAnimal animal(species, name, yearOfBirth, numOfCages, numOfMeals, expectedLifetime);
		animals.push_back(animal);
	}

	for (auto it = animals.begin(); it != animals.end(); it++) {
		for (int j = 0; j < it->GetExpectedLifetime() + 2; j++) {
			Mass newData(it->GetYearOfBirth() + j, rand() % 20 + 10 + j);
			it->AddMassData(newData);
		}
		it->PrintData();
		CheckWeightChange(*it);
		it->PrintData();
		cout << endl;
	}
}