#include <iostream>
#include <algorithm>

using namespace std;

// pretpostavlja se da korisnik unosi redom brojeve od 1 - n s tim da jedan fali
void find_missing(int *arr, int size) {
	// ako nedostaje prvi
	if (arr[0] != 1) {
		arr[size - 1] = 1;
		sort(arr, arr + size);
		return;
	}

	// ako nedostaje zadnji
	if (arr[size - 2] != size) {
		arr[size - 1] = size;
		sort(arr, arr + size);
		return;
	}

	for (int i = 1; i < size - 1; i++) {
		if (arr[i] - arr[i - 1] != 1) {
			arr[size - 1] = arr[i] - 1;
			break;
		}
	}

	sort(arr, arr + size);
}

int main() {
	int *arr;
	int n;

	cout << "Unesite velicinu niza (preporuceno 9): " << endl;
	cin >> n;

	arr = new int[n];

	for (int i = 0; i < n - 1; i++) {
		cout << "Unesite broj (1-9): " << endl;
		cin >> arr[i];
	}

	find_missing(arr, n);

	for (int i = 0; i < n; i++) {
		cout << arr[i] << " ";
	}

	delete[] arr;
	arr = 0;
}