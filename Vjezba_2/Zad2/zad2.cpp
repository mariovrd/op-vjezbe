#include <iostream>
#include <algorithm>

using namespace std;

void print_arr(int *arr, int size) {
	for (int i = 0; i < size; i++) {
		cout << arr[i] << " ";
	}
	cout << endl;
}

void swap(int &a, int &b) {
	int temp = a;
	a = b;
	b = temp;
}

bool is_even(int &a) {
	if ((a % 2) == 0)
		return true;
	return false;
}

void separate_even_odd(int *arr, int size) {
	int c = 0; // number of even numbers (used for sort())

	for (int i = 0; i < size; i++) {
		if (!is_even(arr[i])) {
			for (int j = i + 1; j < size; j++) {
				if (is_even(arr[j])) {
					swap(arr[i], arr[j]);
					c++;
					break;
				}
			}
		}
	}

	sort(arr, arr + c);
	sort(arr + c + 1, arr + size);
}

int main() {
	int size;
	cout << "Unesite velicinu niza: " << endl;
	cin >> size;
	int *arr = new int[size];

	int i;
	int n;
	for (i = 0; i < size; i++) {
		cout << "Unesite broj: " << endl;
		cin >> n;
		arr[i] = n;
	}

	cout << "Nesortirani: ";
	print_arr(arr, size);

	separate_even_odd(arr, size);

	cout << "Sortirani: ";
	print_arr(arr, size);

	delete[] arr;
	arr = 0;
}