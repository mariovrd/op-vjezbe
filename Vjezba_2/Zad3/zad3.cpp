#include <iostream>
#include <vector>

using namespace std;

struct Vector {
	int *elements;
	int logical_size; // koliko ih je unutra
	int physical_size; // koliko je alocirano
};

Vector vector_new(int init) {
	Vector returnVec;
	returnVec.elements = new int[init];
	returnVec.logical_size = 0;
	returnVec.physical_size = init;
	return returnVec;
}

void vector_delete(Vector& theVec) {
	delete[] theVec.elements;
	theVec.logical_size = 0;
	theVec.physical_size = 0;
}

bool vector_empty(Vector& theVec) {
	if (theVec.logical_size == 0)
		return true;
	return false;
}

void print_vector(Vector& theVec) {
	if (vector_empty(theVec))
		return;

	for (int i = 0; i < theVec.logical_size; ++i) {
		cout << theVec.elements[i] << endl;
	}
}

void vector_push_back(Vector& theVec, int num) {
	if (theVec.logical_size == theVec.physical_size) {
		int *new_elements = new int[theVec.physical_size * 2];
		for (int i = 0; i < theVec.physical_size; ++i) {
			new_elements[i] = theVec.elements[i];
		}
		delete[] theVec.elements;
		theVec.elements = new_elements;
		theVec.physical_size = theVec.physical_size * 2;
	}
	theVec.elements[theVec.logical_size] = num;
	theVec.logical_size += 1;
}

void vector_pop_back(Vector& theVec) {
	if (!vector_empty(theVec)) {
		theVec.elements[theVec.logical_size - 1] = 0;
		theVec.logical_size -= 1;
	}
}

int vector_front(Vector& theVec) {
	if (!vector_empty(theVec))
		return theVec.elements[0];
	return -1;
}

int vector_back(Vector& theVec) {
	if (!vector_empty(theVec))
		return theVec.elements[theVec.logical_size - 1];
	return -1;
}

int vector_size(Vector& theVec) {
	return theVec.logical_size;
}

int main() {
	int init;
	cout << "Unesite pocetnu vrijednost vektora: ";
	cin >> init;

	Vector myVec = vector_new(init);

	while (true) {
		int input;
		cout << "Unesite sto zelite napraviti: \n1 - push_back\n2 - pop_back\n3 - front\n4 - back\n" <<
			"5 - print_vector\n6 - delete\n7 - size\n0 - izlaz" << endl;
		cin >> input;

		if (input == 0) break;

		switch (input)
		{
		case 1:
			cout << "Unesite broj koji zelite pushati: ";
			cin >> input;
			vector_push_back(myVec, input);
			break;
		case 2:
			vector_pop_back(myVec);
			break;
		case 3:
			cout << "vector_front: " << vector_front(myVec) << endl;
			break;
		case 4:
			cout << "vector_back: " << vector_back(myVec) << endl;
			break;
		case 5:
			print_vector(myVec);
			break;
		case 6:
			vector_delete(myVec);
			break;
		case 7:
			cout << vector_size(myVec) << endl;
			break;
		default:
			break;
		}
	}

	// test
	//vector_push_back(myVec, 3);
	//vector_push_back(myVec, 7);
	//print_vector(myVec);
	//cout << "vector_front: " << vector_front(myVec) << "\nvector_back: " << vector_back(myVec) << 
	//	"\nvector_size: " << vector_size(myVec) << endl;
	//vector_pop_back(myVec);
	//print_vector(myVec);
	//vector_delete(myVec);
	//print_vector(myVec);

}