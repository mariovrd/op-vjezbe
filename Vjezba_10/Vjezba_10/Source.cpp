#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class MathErr {
public:
	virtual void PrintError() = 0;
	virtual string GetError() = 0;
};

class ZeroDivide : public MathErr {
public:
	void PrintError() { cout << " dijeljenje s nulom " << endl; }
	string GetError() { return " dijeljenje s nulom "; }
};

class UnsupportedOperator : public MathErr {
public:
	void PrintError() { cout << " nepodrzan operator " << endl; }
	string GetError() { return " nepodrzan operator "; }
};

class NoNumber : public MathErr {
public:
	void PrintError() { cout << " broj nije unesen " << endl; }
	string GetError() { return " broj nije unesen "; }
};

class NoOperator : public MathErr {
public:
	void PrintError() { cout << " operator nije unesen " << endl; }
	string GetError() { return " operator nije unesen "; }
};


bool IsOperatorValid(char op) {
	if (op == '+' || op == '-' || op == '*' || op == '/') 
		return true;
	return false;
}

int EnterOperand() {
	cout << "Unesite operand: ";
	int operand;
	cin >> operand;
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
		throw(NoNumber());
	}
	else {
		return operand;
	}
}

char EnterOperator() {
	cout << "Unesite operator: ";
	char op;
	cin >> op;
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
		throw(NoOperator());
	}
	else {
		if (IsOperatorValid(op)) {
			return op;
		}
		else {
			throw(UnsupportedOperator());
		}
	} 

}

// this function must get valid operators and operation
int Calculate(int op1, int op2, char operation) {
	if (operation == '+') {
		return op1 + op2;
	}
	else if (operation == '-') {
		return op1 - op2;
	}
	else if (operation == '*') {
		return op1 * op2;
	}
	else {
		if (op2 == 0) {
			throw(ZeroDivide());
		}
		else {
			return op1 / op2;
		}
	}
}

int main() {
	int op1, op2;
	char operation;
	while (true) {
		try {
			op1 = EnterOperand();
			operation = EnterOperator();
			op2 = EnterOperand();
			cout << Calculate(op1, op2, operation) << endl;
		} 
		catch(MathErr& x) {
			ofstream myfile;
			myfile.open("error.log", ios::out | ios::app);
			myfile << x.GetError() << endl;
			myfile.close();
			x.PrintError();
		}
		catch (...) {
			cout << "some unknown error" << endl;
		}
	}

}