#include <iostream>
#include "Array.h"

using namespace std;
using namespace OOP;

int main() {
	Array a(3), b(3);
	cin >> a;
	cout << a;
	cin >> b;
	cout << b;
	Array c = a + b;
	cout << c;
	Array d = a - b;
	cout << d;
	Array e;
	e = a;
	cout << e;
	cout << "Is a == b? " << (a == b) << endl;
	cout << "Is a != b? " << (a != b) << endl;
	cout << "Element on index 2: " << a[2] << endl;
	a[2] = 5;
	cout << "*changed* Element on index 2: " << a[2] << endl;
	cout << a;
	cout << "Counter: " << a.GetCounter() << endl;
}