#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>

namespace OOP {
	class Array {
	private:
		int * arr;
		unsigned size;
		static int counter;
	public:
		Array();
		Array(unsigned size);
		Array(const Array& other);
		~Array();

		unsigned GetSize() const;
		int GetCounter() const { return counter; }

		Array& operator=(const Array& other);
		bool operator==(const Array& other);
		bool operator!=(const Array& other);
		int& operator[](int i);

		friend Array operator + (const Array& a, const Array& b);
		friend Array operator - (const Array& a, const Array& b);

		friend std::istream& operator>>(std::istream& is, Array& a);
		friend std::ostream& operator<<(std::ostream& os, Array& a);
	};
}

#endif // !ARRAY_H
