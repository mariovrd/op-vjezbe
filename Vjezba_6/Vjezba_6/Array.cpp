#include "Array.h"

using namespace std;

namespace OOP {

	int Array::counter = 0;

	Array::Array() {
		arr = new int [0];
		size = 0;
		++counter;
	}

	Array::Array(unsigned size) {
		arr = new int[size];
		this->size = size;
		++counter;
	}

	Array::Array(const Array& other) {
		size = other.size;
		arr = new int[size];
		++counter;
		for (unsigned i = 0; i < size; i++) {
			arr[i] = other.arr[i];
		}

	}

	Array::~Array() {
		delete[] arr;
		--counter;
	}

	unsigned Array::GetSize() const {
		return size;
	}

	Array& Array::operator=(const Array& other) {
		size = other.size;
		arr = new int[size];
		for (unsigned i = 0; i < size; i++) {
			arr[i] = other.arr[i];
		}
		return *this;
	}

	bool Array::operator==(const Array& other) {
		if (this->size != other.GetSize()) {
			return false;
		}
		for (unsigned i = 0; i < this->size; i++) {
			if (this->arr[i] != other.arr[i])
				return false;
		}
		return true;
	}

	bool Array::operator!=(const Array& other) {
		if (this->size != other.GetSize()) {
			return true;
		}
		for (unsigned i = 0; i < this->size; i++) {
			if (this->arr[i] != other.arr[i])
				return true;
		}
		return false;
	}

	int& Array::operator[](int i) {
		return this->arr[i];
	}

	Array operator+(const Array& a, const Array& b) {
		Array concatenated(a.GetSize() + b.GetSize());
		unsigned i = 0;
		for (i = 0; i < a.GetSize(); i++) {
			concatenated.arr[i] = a.arr[i];
		}
		for (unsigned j = 0; j < b.GetSize(); j++) {
			concatenated.arr[i + j] = b.arr[j];
		}
		return concatenated;
	}

	Array operator-(const Array& a, const Array& b) {
		Array firstNotSecond(a.GetSize());
		bool found;
		int k = 0;
		for (unsigned i = 0; i < a.GetSize(); i++) {
			found = false;
			for (unsigned j = 0; j < b.GetSize(); j++) {
				if (a.arr[i] == b.arr[j]) {
					found = true;
					break;
				}
			}
			if (!found) {
				firstNotSecond.arr[k] = a.arr[i];
				k++;
			}
		}
		firstNotSecond.size = k;
		return firstNotSecond;
	}

	istream& operator>>(istream& is, Array& a) {
		for (unsigned i = 0; i < a.GetSize(); i++) {
			cout << "Enter number: ";
			is >> a.arr[i];
		}
		return is;
	}

	ostream& operator<<(ostream& os, Array& a) {
		for (unsigned i = 0; i < a.GetSize(); i++) {
			os << a.arr[i] << " ";
		}
		os << endl;
		return os;
	}

}