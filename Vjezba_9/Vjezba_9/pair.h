#ifndef PAIR_H
#define PAIR_H
#include <iostream>

using namespace std;

template<typename T1, typename T2>
class Pair
{
	T1 first;
	T2 second;
public:
	//Pair() : first(T1()), second(T2()) {}
	//Pair(const T1& t1, const T2& t2) : first(t1), second(t2) {}
	Pair(const T1& t1 = T1(), const T2& t2 = T2()) : first(t1), second(t2) {}
	Pair(const Pair<T1, T2>& other) : first(other.first), second(other.second) {}



	bool operator== (const Pair<T1, T2>& other) const
	{
		return first == other.first && second == other.second;
	}
	bool operator!= (const Pair<T1, T2>& other) const
	{
		return first != other.first && second != other.second;
	}
	bool operator>= (const Pair<T1, T2>& other) const
	{
		return first >= other.first && second >= other.second;
	}
	bool operator> (const Pair<T1, T2>& other) const
	{
		return first > other.first && second > other.second;
	}
	bool operator<= (const Pair<T1, T2>& other) const
	{
		return first <= other.first && second <= other.second;
	}
	bool operator< (const Pair<T1, T2>& other) const
	{
		return first < other.first && second < other.second;
	}

	Pair<T1, T2>& operator= (const Pair<T1, T2>& other) {
		first = other.first;
		second = other.second;
		return *this;
	}

	friend istream& operator >> (istream &is, Pair<T1, T2> &p) {
		is >> p.first;
		is >> p.second;
		return is;
	}
	friend ostream& operator << (ostream &os, const Pair<T1, T2> &p) {
		os << "(" << p.first << ", " << p.second << ") ";
		return os;
	}

	void swap() {
		Pair<T1, T2> tmp = *this;
		first = tmp.second;
		second = tmp.first;
	}
};

template <>
class Pair <char*, char*> {
	char* first;
	char* second;
public:
	//Pair() : first(T1()), second(T2()) {}
	//Pair(const T1& t1, const T2& t2) : first(t1), second(t2) {}
	Pair(char *c1 = new char, char *c2 = new char) : first(c1), second(c2) {}

	bool operator> (const Pair<char*, char*>& other) const
	{
		return first > other.first && second > other.second;
	}
	bool operator< (const Pair<char*, char*>& other) const
	{
		return first < other.first && second < other.second;
	}

	Pair<char*, char*>& operator= (const Pair<char*, char*>& other) {
		first = other.first;
		second = other.second;
		return *this;
	}

	friend istream& operator >> (istream &is, Pair<char*, char*> &p) {
		is >> p.first;
		is >> p.second;
		return is;
	}
	friend ostream& operator << (ostream &os, const Pair<char*, char*> &p) {
		os << "(" << p.first << ", " << p.second << ") ";
		return os;
	}


};

#endif // !PAIR_H