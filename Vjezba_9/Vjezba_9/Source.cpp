#include <iostream>
#include <vector>
#include <algorithm>
#include "pair.h"

int main()
{
	//Pair<int, int> p1;
	//cin >> p1;
	//cout << p1;
	//p1.swap();
	//cout << p1;
	using namespace std;
	Pair<char*, char*> p1, p2, p3;
	vector<Pair<char*, char*> > v;

	cin >> p1 >> p2 >> p3;

	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);

	sort(v.begin(), v.end());

	vector<Pair<char*, char*> >::iterator it;
	for (it = v.begin(); it != v.end(); ++it)
		cout << *it << endl;

}