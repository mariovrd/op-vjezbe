#ifndef POINT_H
#define POINT_H

class Point {
private:
	double x;
	double y;
	double z;

public:
	void SetPoint(double x = 0.0, double y = 0.0, double z = 0.0);
	void SetPointRandom(int x = 1, int y = 1, int z = 1);

	double GetX() const;
	double GetY() const;
	double GetZ() const;

	double CalcDistance2D(Point& other) const;
	double CalcDistance3D(Point& other) const;

	void Print() const;
};

#endif