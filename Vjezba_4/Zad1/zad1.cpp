#include "point.h"
#include <ctime>
#include <iostream>

using namespace std;

int main() {
	srand(time(NULL));
	Point p1, p2;
	p1.SetPoint();
	p2.SetPointRandom(3, 5, 6);
	p1.Print();
	p2.Print();
	cout << "2D udaljenost od p1 do p2 je " << p1.CalcDistance2D(p2) << endl;
	cout << "3D udaljenost od p1 do p2 je " << p1.CalcDistance3D(p2) << endl;
}