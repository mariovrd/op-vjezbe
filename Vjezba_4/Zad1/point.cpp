#include <iostream>
#include <ctime>
#include <cmath>
#include "point.h"

using namespace std;

void Point::SetPoint(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

void Point::SetPointRandom(int x, int y, int z) {
	this->x = round(rand() % x * 20 - x) - x;
	this->y = round(rand() % y * 20 - y) - y;
	this->z = round(rand() % z * 20 - z) - z;
}

double Point::GetX() const {
	return x;
}

double Point::GetY() const {
	return y;
}

double Point::GetZ() const {
	return z;
}

double Point::CalcDistance2D(Point& other) const {
	return sqrt(pow((other.GetX() - this->GetX()), 2) + pow((other.GetY() - this->GetY()), 2));
}

double Point::CalcDistance3D(Point& other) const {
	return sqrt(pow((other.GetX() - this->GetX()), 2) + pow((other.GetY() - this->GetY()), 2) + pow((other.GetZ() - this->GetZ()), 2));
}

void Point::Print() const {
	cout << "x: " << this->GetX() << "\ty: " << this->GetY() << "\tz: " << this->GetZ() << endl;
}
