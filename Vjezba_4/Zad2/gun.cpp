#include <iostream>
#include "gun.h"

using namespace std;

void Gun::SetPosition(double x, double y, double z) {
	this->position.SetPoint(x, y, z);
}

void Gun::SetMaxBullets(unsigned max) {
	this->maxBullets = max;
}

void Gun::SetBulletsIn(unsigned inside) {
	this->bulletsIn = inside;
}

double Gun::GetPositionX() const {
	return position.GetX();
}

double Gun::GetPositionY() const {
	return position.GetY();
}

double Gun::GetPositionZ() const {
	return position.GetZ();
}

unsigned Gun::GetMaxBullets() const {
	return maxBullets;
}

unsigned Gun::GetBulletsIn() const {
	return bulletsIn;
}


void Gun::Shoot() {
	if (this->bulletsIn == 0) {
		cout << "You don't have any bullets. Do you want to reload? (1 - yes | 0 - no) " << endl;
		int reload = 0;
		cin >> reload;
		if (reload)
			this->Reload();
		else
			return;
	}

	this->bulletsIn--;
	cout << "BANG" << endl;
	cout << "You have " << this->bulletsIn << " bullets remaining." << endl;
}

void Gun::Reload() {
	cout << "Reloading... ";
	this->bulletsIn = this->maxBullets;
	cout << "You have " << this->bulletsIn << " bullets." << endl;
}

void Gun::PrintPosition() const {
	cout << "--- GUN POSITION ---" << endl;
	this->position.Print();
}