#ifndef GUN_H
#define GUN_H

#include "../Zad1/point.h"

class Gun {
private:
	Point position;
	unsigned maxBullets;
	unsigned bulletsIn;

public:
	void SetPosition(double x = 0, double y = 0, double z = 0);
	void SetMaxBullets(unsigned max);
	void SetBulletsIn(unsigned inside = 0);

	double GetPositionX() const;
	double GetPositionY() const;
	double GetPositionZ() const;
	unsigned GetMaxBullets() const;
	unsigned GetBulletsIn() const;

	void Shoot();
	void Reload();

	void PrintPosition() const;
};

#endif 