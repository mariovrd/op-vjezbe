#include <iostream>
#include "gun.h"

using namespace std;

int main() {
	Gun gun;
	gun.SetPosition(1, 1, 1);
	gun.SetMaxBullets(7);
	gun.SetBulletsIn(7);
	gun.PrintPosition();

	int input = 0;
	while (true) {
		cout << "Do you want to shoot? (1 - yes | 0 - no) ";
		cin >> input;
		if (!input)
			break;

		gun.Shoot();
	}
}