#include <iostream>
#include <vector>
#include <ctime>
#include "../Zad3/target.h"

using namespace std;

vector<Target> GenerateTargets(unsigned numOfTargets) {
	vector<Target> targets;
	for (int i = 0; i < numOfTargets; i++) {
		Target newTarget;
		newTarget.SetRandomPosition(i+1, i+1, i+1);
		newTarget.SetHeight(20);
		newTarget.SetWidth(10);
		targets.push_back(newTarget);
	}
	return targets;
}

int main() {
	srand(time(NULL));
	unsigned n;
	cout << "Enter how many targets you want to generate: ";
	cin >> n;

	Gun gun;
	gun.SetPosition();
	gun.SetMaxBullets(7);
	gun.SetBulletsIn(7);
	vector<Target> targets = GenerateTargets(n);

	int targetsHitted = 0;

	for (auto it = targets.begin(); it != targets.end(); it++) {
		if (gun.GetBulletsIn() == 0) 
			break;
		it->ShootAtTargets360(gun);
		if (it->GetState())
			targetsHitted++;
	}

	cout << "You've hitted " << targetsHitted << " targets." << endl;
}