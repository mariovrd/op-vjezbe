#include <iostream>
#include "target.h"

using namespace std;

void Target::SetPosition(double x, double y, double z) {
	this->position.SetPoint(x, y, z);
}

void Target::SetRandomPosition(int x, int y, int z) {
	this->position.SetPointRandom(x, y, z);
}

void Target::SetWidth(unsigned width) {
	this->width = width;
}

void Target::SetHeight(unsigned height) {
	this->height = height;
}

double Target::GetPositionX() const {
	return this->position.GetX();
}

double Target::GetPositionY() const {
	return this->position.GetY();
}

double Target::GetPositionZ() const {
	return this->position.GetZ();
}

unsigned Target::GetWidth() const {
	return this->width;
}

unsigned Target::GetHeight() const {
	return this->height;
}

bool Target::GetState() const {
	return this->targetHit;
}

void Target::ShootAtTarget(Gun& gun) {
	if (gun.GetBulletsIn() == 0) {
		cout << "You don't have any bullets. Do you want to reload? (1 - yes | 0 - no) " << endl;
		int reload = 0;
		cin >> reload;
		if (reload)
			gun.Reload();
		else
			return;
	}

	gun.Shoot();

	if (gun.GetPositionY() > this->GetPositionY()) {
		cout << "Gun was behind the target so it couldn't hit a target." << endl;
		this->targetHit = false;
	}
	else if (gun.GetPositionX() > this->GetPositionX() && gun.GetPositionX() < this->GetPositionX() + this->GetWidth()
		&& gun.GetPositionZ() > this->GetPositionZ() && gun.GetPositionZ() < this->GetPositionZ() + this->GetHeight()) {
		cout << "You've hit the target." << endl;
		this->targetHit = true;
	}
	else {
		cout << "You've missed the target." << endl;
		this->targetHit = false;
	}
}

void Target::ShootAtTargets360(Gun& gun) {
	if (gun.GetBulletsIn() == 0) {
		cout << "You don't have any bullets." << endl;
		return;
	}
	
	if (gun.GetBulletsIn() > 0) {
		gun.Shoot();
		if (gun.GetPositionZ() > this->GetPositionZ() && gun.GetPositionZ() < this->GetPositionZ() + this->GetHeight()
			&& this->targetHit == false) {
			cout << "You've hit the target." << endl;
			this->targetHit = true;
		}
		else {
			cout << "You've missed the target." << endl;
			this->targetHit = false;
		}
	}
}

void Target::PrintPosition() const {
	cout << "--- TARGET POSITION ---\n Top left: " << this->GetPositionX() + this->GetHeight() + this->GetPositionZ() <<
		"\n Top right: " << this->GetPositionX() + this->GetWidth() + this->GetPositionZ() <<
		"\n Bottom left: " << this->GetPositionX() <<
		"\n Bottom right: " << this->GetPositionX() + this->GetWidth() << 
		"\n Y: " << this->GetPositionY() << endl;
}