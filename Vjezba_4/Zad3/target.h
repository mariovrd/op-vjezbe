#ifndef TARGET_H
#define TARGET_H

#include "../Zad1/point.h"
#include "../Zad2/gun.h"

class Target {
private:
	Point position;
	unsigned width;
	unsigned height;
	bool targetHit = false;

public:
	void SetPosition(double x = 0, double y = 0, double z = 0);
	void SetRandomPosition(int x, int y, int z);
	void SetWidth(unsigned width);
	void SetHeight(unsigned height);

	double GetPositionX() const;
	double GetPositionY() const;
	double GetPositionZ() const;
	unsigned GetWidth() const;
	unsigned GetHeight() const;

	bool GetState() const;

	void ShootAtTarget(Gun& gun);
	void ShootAtTargets360(Gun& gun);

	void PrintPosition() const;
};

#endif 