#include <iostream>
#include <ctime>
#include "target.h"

using namespace std;

int main() {
	srand(time(NULL));
	Target target;
	target.SetPosition();
	target.SetHeight(10);
	target.SetWidth(20);

	Gun gun;
	gun.SetMaxBullets(5);
	gun.SetBulletsIn(1);
	for (int i = 0; i < 20; i++) {
		target.SetPosition(rand() % 20, rand() % 25, rand() % 23);
		gun.SetPosition(rand() % 20, rand() % 5, rand() % 23);
		target.ShootAtTarget(gun);
	}
}